.. _api:

All Functions
=============

This section provides the prototypes and descriptions of all functions    
integrated into CPC library.
These functions allow you to control CPC.

.. warning::

   More or less functions are available according to the device type.  
   The compatibility depends on the device part number recovered by
   the "CPC_getSystemVersion" function.                      
   Please see notes functions to check the compatibility with your device:    
   -> version compatibility:  PN_CPC_x_xx_xx_xx   
   Refer to section :ref:`Code Examples`, to recover version in C++ or in Python.

.. toctree::
   :maxdepth: 2
   :glob:

   *