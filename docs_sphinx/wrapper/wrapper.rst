.. _wrapper:

C++ Wrapper
===========

Wrapper Advantage
~~~~~~~~~~~~~~~~~

A C++ wrapper has been created for several reasons.
	- To make CPC functions easy to use.
	- To allow multiple CPC device control in the same application and at the same time.
	- To link Dynamic Library inside C++ code and not in the project configuration.

.. note::

	Except OpenDevice function, you do not need to specify iDev when using CPC wrapper function.

	For example function ``CPC_getCLKCountData(short iDev, unsigned long *CLK, unsigned long *Count)`` can be replace by ``ObjectName.GetCLKCountData(unsigned long *CLK, unsigned long *Count)``

C++ code
~~~~~~~~

Here is an example of how to use this wrapper to recover data from 2 CPC :

.. code-block:: c++

	#include <iostream>
	using namespace std;

	#include "CPC_wrapper.h"
	#include "CPC.h"

	// Select shared library compatible to current operating system
	#ifdef _WIN32
	#define DLL_PATH L"CPC.dll"
	#elif __unix
	#define DLL_PATH "CPC.so"
	#else
	#define DLL_PATH "CPC.dylib"
	#endif

	int main(int argc, const char* argv[]) {
		short iDev = 0;
		short ret;
		char* devicesList[10];
		short numberDevices;
		unsigned long CLK = 0, Count = 0;
		unsigned long CLK2 = 0, Count2 = 0;

		// Instancie the device from wrapper
		CPC_wrapper CPC0(DLL_PATH);
		CPC_wrapper CPC1(DLL_PATH);

	    /*    ListDevices function    */
	    // List Aurea Technology devices: MANDATORY BEFORE EACH OTHER ACTION ON THE SYSTEM
	    if (CPC0.ListDevices(devicesList, &numberDevices) == 0) { 
	        if (numberDevices == 0){            
	            cout << endl << "    Please connect AT device !" << endl << endl;
	            do {
	                delay(500);
	                CPC0.ListDevices(devicesList, &numberDevices);
	            } while (numberDevices == 0);
	        }
	    }

		// Open communication with device 0
		printf(" -%u: %s\n", 0, devicesList[0]);
		CPC0.OpenDevice(0);
		printf("\n CPC %d-> Communication Open\n\n", 0);

		// Open communication with device 1
		printf(" -%u: %s\n", 1, devicesList[1]);
		CPC1.OpenDevice(1);
		printf("\n CPC %d-> Communication Open\n\n", 1);

		// Recover Clock and Photons count for both devices
		CPC0.GetCLKCountData(&CLK, &Count);
		printf("\n\nCPC 0 -> Clock: %7lu	Hz	Counts : %7lu", CLK, Count);
		CPC1.GetCLKCountData(&CLK2, &Count2);
		printf("\nCPC 1 -> Clock: %7lu	Hz	Counts : %7lu", CLK2, Count2);

		// Wait some time
		delay(2000);

		/*    CloseDevice function    */
		// Close initial device opened: MANDATORY AFTER EACH END OF SYSTEM COMMUNICATION.
		if (CPC0.CloseDevice() == 0) cout << "   -> Communication closed" << endl;
		else cout << "  -> Failed to close communication" << endl;
		if (CPC1.CloseDevice() == 0) cout << "   -> Communication closed" << endl;
		else cout << "  -> Failed to close communication" << endl;

		// Call class destructor
		CPC0.~CPC_wrapper();
		CPC1.~CPC_wrapper();

		return 0;
	}
