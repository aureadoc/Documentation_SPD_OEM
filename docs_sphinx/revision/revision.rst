.. _revision:

Revision History
================

v4.02 (22/02/22)
----------------

- Fix error when settings deadtime range

v4.01 (24/11/22)
----------------

- Change libusb interface index to open multiple devices

v4.00 (05/05/22)
----------------
    - Add Device index in all functions
    - Modify all get functions to return value by pointer
    - Manage multiple device utilization in the same program
    - Add CPC_ at the beginning of all functions
    - Replace functions:
    	- CPC_listATdevices() to CPC_listDevices()
    	- CPC_openATdevice() to CPC_openDevice()
   		- CPC_closeATdevice() to CPC_closeDevice()
    - Add Wrapper
    - Handle multiple device application

v3.11 (23/06/21)
----------------

- Add Python examples

v3.10 (08/01/21)
----------------

- Add new output format

v3.09 (27/02/20)
----------------

- couples of updates on library

v3.08 (04/12/19)
----------------

- Update efficiency limits

v3.07 (25/03/19)	
----------------

- Modification functions:
		- SetOutputFormat(): add NIM format 
		- GetOutputFormat(): add NIM format
- Modifications of function compatibilities and comments

v3.06 (03/03/19)	
----------------

- Rename functions:
	- SetVisibleModuleDetection() to SetDetectionMode()
	- GetVisibleModuleDetection() to GetDetectionMode()

v3.05 (26/02/19)
----------------

- Improvement of functions:
	- setDeadtime() (accuracy to the thousandth)

v3.04 (02/08/17)	
----------------

- Improvement of the device closing

v3.03 (12/01/17)
----------------

- Improvement of internals functions
   to close the DLL cleaner way 

v3.02 (11/01/17)	
----------------

- Internal improvement

v3.01 (23/12/16)
----------------

- Improvement GetCLKCountData() function
    - Add functions:
    	- GetOutputVoltage()

v3.00 (19/03/15)
----------------

- Replace functions:
	- OpenSystemTransfer() to OpenATdevice()
	- CloseSystemTransfer() to CloseATdevice()


v2.01 (02/03/15)
----------------

- Update library comments


v2.00 (22/09/14)
----------------

- Add functions:
	- SetOutputFormat()
	- GetOutputFormat()
	- SetIntegTime()
	- GetIntegTime()
	- SetAnalogOutGain()
	- GetAnalogOutGain()


v1.00 (21/05/14)
----------------

- First release
